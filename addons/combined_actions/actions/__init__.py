"""This package contains all the addon proxy's actions."""
from .clearandtypeaction import ClearAndTypeAction

__all__ = ["ClearAndTypeAction"]
